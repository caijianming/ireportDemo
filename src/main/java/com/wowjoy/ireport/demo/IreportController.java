package com.wowjoy.ireport.demo;

import com.wowjoy.ireport.entitys.Person;
import com.wowjoy.ireport.mappers.PersonMapper;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by caijianming on 2017/5/25 0025.
 */
@Controller
@RequestMapping("/")
public class IreportController {

  @Autowired
  private Environment environment;

  @Autowired
  private PersonMapper mapper;

  @Autowired
  private ApplicationContext applicationContext;


    /**
     * 无参构造器
     */
  public IreportController() {

  }

    /**
     * 跳转index页面
     * @param model
     * @return
     */
  @RequestMapping(value = "index",method = RequestMethod.GET)
  public String index(ModelMap model){

      model.put("hello","hello this is ireport demo!");
      return "index";
  }

    /**
     * 查询所有人员
     * @return
     */
  @RequestMapping(value = "person/findAll",method = RequestMethod.GET)
  public String findAll(@RequestParam(required = false) Map<String,Object> param, ModelMap model){
      // 报表数据源
      JRDataSource jrDataSource = new JRBeanCollectionDataSource(mapper.queryPersonList());

      // 动态指定报表模板url
      model.addAttribute("url", "classpath:ireport/MvcIReportExample.jasper");
      model.addAttribute("format", "html"); // 报表格式
      model.addAttribute("jrMainDataSource", jrDataSource);

      return "ReportView";
  }

    public static void main(String[] args) {

    }
}
