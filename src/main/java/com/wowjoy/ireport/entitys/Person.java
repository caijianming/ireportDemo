package com.wowjoy.ireport.entitys;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户实体类
 * Created by caijianming on 2017/5/25 0025.
 */
public class Person {


    private Integer id;

    private String name;

    private String sex;

    private Integer age;

    private String address;


    public Person(Integer id, String name, String sex, Integer age, String address) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static List<Person> getList(){
        List<Person> result = new ArrayList<>();

        result.add(new Person(10001,"张三","男",20,"北京市"));
        result.add(new Person(10002,"李四","男",25,"上海市"));
        result.add(new Person(10003,"王五","男",28,"南京市"));
        result.add(new Person(10004,"赵六","男",21,"北京市"));
        result.add(new Person(10005,"李雷","男",30,"北京市"));
        result.add(new Person(10006,"韩梅梅","女",29,"西安市"));
        result.add(new Person(10007,"赵瑞龙","男",45,"北京市"));
        result.add(new Person(10008,"侯亮平","男",35,"北京市"));
        result.add(new Person(10009,"郑成功","男",40,"京州市"));
        result.add(new Person(10010,"高育良","男",60,"京州市"));
        result.add(new Person(10011,"李达康","男",55,"京州市"));
        result.add(new Person(10012,"沙瑞金","男",65,"京州市"));
        return result;
    }
}
