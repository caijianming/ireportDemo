package com.wowjoy.ireport.mappers;

import com.wowjoy.ireport.entitys.Person;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Administrator on 2017/5/25 0025.
 */
@Mapper
public interface PersonMapper {

    @Options(useGeneratedKeys = true, keyProperty = "id") //回写自增的主键ID  
    @Insert("insert into person (name,sex,age,address)values(#{name},#{sex},#{age},#{address})")
    public Integer addPerson(Person person);

    @Delete("delete from person where id=#{0}")
    public Integer deletePersonById(Integer id);

    @Update("update person set name=#{name},sex=#{sex},age=#{age},address=#{address} where id=#{id}")
    public Integer updatePerson(Person Person);

    @Select("select * from person where id=#{0}")
    public Person getById(Integer id);

    @Select("select * from person")
    public List<Person> queryPersonList();
}
