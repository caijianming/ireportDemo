package com.wowjoy.ireport.util;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;

import java.util.Map;

/**
 * Created by Administrator on 2017/5/26 0026.
 */
@Component("ReportView")
public class ReportView extends JasperReportsMultiFormatView {

    private JasperReport jasperReport;

    public ReportView() {
        super();
    }

    protected JasperPrint fillReport(Map<String, Object> model) throws Exception {
        if (model.containsKey("url")) {
            setUrl(String.valueOf(model.get("url")));
            this.jasperReport = loadReport();
        }

        return super.fillReport(model);
    }

    protected JasperReport getReport() {
        return this.jasperReport;
    }
}
