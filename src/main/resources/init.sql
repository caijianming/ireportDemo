DROP TABLE PERSON if exists;

create table if not exists person (id int not null primary key auto_increment,
name varchar(50),sex varchar(50),age int,address varchar(100));

insert into person (name,sex,age,address)values('张三','男',20,'北京市');
insert into person (name,sex,age,address)values('李四','男',25,'上海市');
insert into person (name,sex,age,address)values('王五','男',28,'南京市');
insert into person (name,sex,age,address)values('赵六','男',21,'北京市');
insert into person (name,sex,age,address)values('李雷','男',30,'北京市');
insert into person (name,sex,age,address)values('韩梅梅','女',29,'西安市');
insert into person (name,sex,age,address)values('赵瑞龙','男',45,'北京市');
insert into person (name,sex,age,address)values('侯亮平','男',35,'北京市');
insert into person (name,sex,age,address)values('郑成功','男',40,'京州市');
insert into person (name,sex,age,address)values('高育良','男',60,'京州市');
insert into person (name,sex,age,address)values('李达康','男',55,'京州市');
insert into person (name,sex,age,address)values('沙瑞金','男',65,'京州市');